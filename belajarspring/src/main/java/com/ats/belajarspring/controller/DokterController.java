package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.DokterModel;
import com.ats.belajarspring.service.DokterService;

@Controller
public class DokterController {

	@Autowired
	private DokterService dokterService;
	
	@RequestMapping(value="menu_tambah_dokter")
	public String menuTambahDokter() {
		String html = "dokter/tambah_dokter";
		return html;
	}
	
	@RequestMapping(value="proses_tambah_dokter")
	public String prosesTambahDokter(HttpServletRequest request, Model model) {
		String gelarDokter = request.getParameter("gelar");
		String namaDokter = request.getParameter("nama");
		//int idDokter = Integer.valueOf(request.getParameter("id"));
		
		DokterModel dokterModel = new DokterModel();
		
		dokterModel.setNamaDokter(namaDokter);
		dokterModel.setGelarDokter(gelarDokter);
		//dokterModel.setIDokter(idDokter);
		
		dokterService.create(dokterModel);// buat masuk ke database
		
		model.addAttribute("dokterModel", dokterModel); // lempar ke frontend
		
		String html = "dokter/success";
		return html;
	}
	@RequestMapping(value="tampil_dokter")
	
	public String TampilDokter( Model model) {
		List<DokterModel> dokterModels = new ArrayList<DokterModel>();
		dokterModels = dokterService.read();
		model.addAttribute("dokterModels", dokterModels);
		String html = "dokter/tampil_dokter";
		return html;
	}

	
}
