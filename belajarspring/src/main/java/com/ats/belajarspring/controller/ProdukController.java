//package com.ats.belajarspring.controller;
//
//import java.util.ArrayList;
//
//
//import java.util.List;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import com.ats.belajarspring.model.ProdukModel;
//import com.ats.belajarspring.service.ProdukService;
//
//
//@Controller
//public class ProdukController {
//	
//	@Autowired
//	private ProdukService produkService;
//
//	@RequestMapping(value="menu_tambah_produk")
//	public String menuTambahProduk() {
//		String html = "produk/tambah_produk";
//		return html;
//	}
//	
//	@RequestMapping(value="proses_tambah_produk")
//	public String prosesTambahProduk(HttpServletRequest request, Model model) {
//		String kodeProduk = request.getParameter("kode");
//		String namaProduk = request.getParameter("nama");
//		int hargaProduk = Integer.valueOf(request.getParameter("harga"));
//		
//		ProdukModel produkModel = new ProdukModel();
//		
//		produkModel.setKodeProduk(kodeProduk);
//		produkModel.setNamaProduk(namaProduk);
//		produkModel.setHargaProduk(hargaProduk);
//		
//		produkService.create(produkModel); // buat masuk ke database
//		
//		model.addAttribute("produkModel", produkModel); // lempar ke frontend
//		
//		String html = "produk/success";
//		return html;
//	}
//	@RequestMapping(value="tampil_produk")
//	
//	public String TampilProduk(HttpServletRequest request, Model model) {
//		List<ProdukModel> produkModels = new ArrayList<ProdukModel>();
//		produkModels = produkService.read();
//		model.addAttribute("produkModels", produkModels);
//		String html = "produk/tampil_produk";
//		return html;
//	}
//	
//	@RequestMapping(value="cari_produk")
//	public String CariProduk(HttpServletRequest request, Model model) {
//		List<ProdukModel> produkModels = new ArrayList<ProdukModel>();
//		produkModels = produkService.search(null);
//		
//		model.addAttribute("produkModel",produkModels);
//		
//		String html = "produk/cari_produk";
//		return html;
//	}
//	
//	@RequestMapping(value="daftar_produk")
//	public String menuDaftarProduk(Model model) {
//		List<ProdukModel> produkModels = new ArrayList<ProdukModel>();
//		produkModels = produkService.read();
//		model.addAttribute("produkModels", produkModels);
//		String html = "produk/daftar_produk";
//		return html;
//	}
//	
//	@RequestMapping(value="menu_urut_asc_produk")
//	public String UrutAscProduk(Model model) {
//		List<ProdukModel> produkModels = new ArrayList<ProdukModel>();
//		produkModels = produkService.asc();
//		model.addAttribute("produkModels", produkModels);
//		String html = "produk/urut_asc_produk";
//		return html;
//	}
//}
