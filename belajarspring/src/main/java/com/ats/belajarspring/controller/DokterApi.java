package com.ats.belajarspring.controller;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.DokterModel;
import com.ats.belajarspring.service.DokterService;

@RestController
@RequestMapping(value="/api/dokterApi")
public class DokterApi {

	@Autowired
	private DokterService dokterService;
	
	@PostMapping("/post")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> dokterpostApi (@RequestBody DokterModel dokterModel){
		dokterService.create(dokterModel);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Berhasil", Boolean.TRUE);
		map.put("Notifikasi", "Berhasil di input");
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code = HttpStatus.OK)
	List<DokterModel> getApi(){
		List<DokterModel> dokterModels = new ArrayList<DokterModel>();
		dokterModels = dokterService.read();
		return dokterModels;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> dokterput (@RequestBody DokterModel dokterModel){
		dokterService.update(dokterModel);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Yeah", Boolean.TRUE);
		map.put("Notifikasi", "Berhasil Di Update");
		return map;
	}
	
	@DeleteMapping("/delete/{iDokter}")
	@ResponseStatus(code = HttpStatus.GONE)
	public Map<String, Object> dokterdelete (@PathVariable String iDokter){
		dokterService.delete(iDokter);
		Map<String, Object> map = new HashMap<String, Object>();
	// map<int, obj> map = new hashmap<int, obj>(); ---- u
		map.put("Selamat", Boolean.TRUE);
		map.put("pesan", "data dengan id" +iDokter+ "berhasil di hapus");
		return map;
	}
	
	@GetMapping("/urut")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DokterModel>urut(){
		List<DokterModel> dokterModels = new ArrayList<DokterModel>();
		dokterModels = dokterService.urutNama();
		return dokterModels;
	}
	
	@GetMapping("/seratus")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DokterModel>seratus(){
		List<DokterModel> dokterModels = new ArrayList<DokterModel>();
		dokterModels = dokterService.seratus();
		return dokterModels;
	}
}
