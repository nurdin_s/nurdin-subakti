package com.ats.belajarspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TrisaktiController {
	
	@RequestMapping(value="menuidentitas")
	public String methodPanggilIdentitas() {
		String html = "trisakti/identitas";
		return html;
	}
	
	@RequestMapping(value="trisaktihasil")
	public String methodPanggilHasil(HttpServletRequest request, Model model) {
		String nama = request.getParameter("nama");
		String tempat = request.getParameter("tempat");
		String tanggal = request.getParameter("tanggal");
		String kelamin = request.getParameter("kelamin");
		String agama = request.getParameter("agama");
		String telp = request.getParameter("telp");
		String hp = request.getParameter("hp");
		String kwn = request.getParameter("kwn");
		String nik = request.getParameter("nik");
		String gol = request.getParameter("gol");
		String tinggal = request.getParameter("tinggal");
		String anak = request.getParameter("anak");
		String dari = request.getParameter("dari");
		String status = request.getParameter("status");
		
		System.out.println(nama);
		System.out.println(tempat);
		System.out.println(tanggal);
		System.out.println(kelamin);
		System.out.println(agama);
		System.out.println(telp);
		System.out.println(hp);
		System.out.println(kwn);
		System.out.println(nik);
		System.out.println(gol);
		System.out.println(tinggal);
		System.out.println(anak);
		System.out.println(dari);
		System.out.println(status);
		
		model.addAttribute("hasil_nama", nama);
		model.addAttribute("hasil_tempat", tempat);
		model.addAttribute("hasil_tanggal", tanggal);
		model.addAttribute("hasil_kelamin", kelamin);
		model.addAttribute("hasil_agama", agama);
		model.addAttribute("hasil_telp", telp);
		model.addAttribute("hasil_hp", hp);
		model.addAttribute("hasil_kwn", kwn);
		model.addAttribute("hasil_nik", nik);
		model.addAttribute("hasil_gol",gol);
		model.addAttribute("hasil_tinggal", tinggal);
		model.addAttribute("hasil_anak", anak);
		model.addAttribute("hasil_dari", dari);
		model.addAttribute("hasil_status", status);
		String html = "trisakti/hasil";
		return html;
	}
	
}
