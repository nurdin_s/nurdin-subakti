package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.PresidenModel;
import com.ats.belajarspring.service.PresidenService;
@RestController
@RequestMapping("api/presidenApi")
public class PresidenApi {

	@Autowired
	private PresidenService presidenService;
	
	@PostMapping("/post")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> postApi (@RequestBody PresidenModel presidenModel){
		presidenService.create(presidenModel);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Berhasil", Boolean.TRUE);
		map.put("Notifikasi", "Berhasil di input");
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code = HttpStatus.OK)
	List<PresidenModel> getApi(){
		List<PresidenModel> presidenModels = new ArrayList<PresidenModel>();
		presidenModels = presidenService.read();
		return presidenModels;
	}
}
