package com.ats.belajarspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class FilmController {
	
	@RequestMapping(value="menutambahdata")
	public String panggil() {
		String html = "film/tambahdata" ;
		return html;
	}
	
	@RequestMapping(value="listfilm")
	public String hasil(HttpServletRequest request, Model model) {
		String kodeFilm = request.getParameter("kodeFilm");
		String namaFilm = request.getParameter("namaFilm");
		String genre = request.getParameter("genre");
		String artis = request.getParameter("artis");
		String produser = request.getParameter("produser");
		String pendapatan = request.getParameter("pendapatan");
		String penghasilan = request.getParameter("penghasilan");
		
		System.out.println(kodeFilm);
		System.out.println(namaFilm);
		System.out.println(genre);
		System.out.println(artis);
		System.out.println(produser);
		System.out.println(pendapatan);
		System.out.println(penghasilan);
		
		
		String html = "film/listfilm";
		return html;
	}
}
