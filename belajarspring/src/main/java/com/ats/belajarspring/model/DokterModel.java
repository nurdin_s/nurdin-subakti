package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_DOKTER")
public class DokterModel {

	@Id
	@Column(name="id")
	private int IDokter;
	@Column(name="nama")
	private String namaDokter;
	@Column(name="gelar")
	private String gelarDokter;
	
	public int getIDokter() {
		return IDokter;
	}
	public void setIDokter(int iDokter) {
		IDokter = iDokter;
	}
	public String getNamaDokter() {
		return namaDokter;
	}
	public void setNamaDokter(String namaDokter) {
		this.namaDokter = namaDokter;
	}
	public String getGelarDokter() {
		return gelarDokter;
	}
	public void setGelarDokter(String gelarDokter) {
		this.gelarDokter = gelarDokter;
	}
	
}
