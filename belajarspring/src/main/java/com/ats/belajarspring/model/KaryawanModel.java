package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_KARYAWAN")
public class KaryawanModel {

	@Id
	@Column(name="NAMA")
	private String namaKaryawan;
	
	@Column(name="MASA JABATAN")
	private int lamaBekerja;
	
	@Column(name="JABATAN")
	private String jabatan;
	
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	public int getLamaBekerja() {
		return lamaBekerja;
	}
	public void setLamaBekerja(int lamaBekerja) {
		this.lamaBekerja = lamaBekerja;
	}
	public String getJabatan() {
		return jabatan;
	}
	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}
}
