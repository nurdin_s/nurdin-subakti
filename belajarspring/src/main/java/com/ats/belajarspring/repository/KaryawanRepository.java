package com.ats.belajarspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ats.belajarspring.model.KaryawanModel;

public interface KaryawanRepository extends JpaRepository<KaryawanModel, String> {

}
