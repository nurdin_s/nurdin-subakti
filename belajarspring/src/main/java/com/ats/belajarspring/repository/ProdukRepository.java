//package com.ats.belajarspring.repository;
//
//import java.util.List;
//
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//
//import com.ats.belajarspring.model.ProdukModel;
//
//public interface ProdukRepository extends JpaRepository<ProdukModel, String> {
//	
//	@Query("SELECT P FROM ProdukModel P ORDER BY P.hargaProduk ASC")
//	List<ProdukModel> hargaProdukOrderAsc();
//	
//	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen like %?1")
//	List<ProdukModel> QuerySelectAllWhereNama(String katakunci_nama);
//}
