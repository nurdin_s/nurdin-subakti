package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.PresidenModel;

public interface PresidenRepository extends JpaRepository<PresidenModel, String>{
	
	@Query("SELECT D FROM PresidenModel D ORDER BY D.namaPresidenDESC")
	List<PresidenModel> SelectAllOrderNamaDESC();
}
