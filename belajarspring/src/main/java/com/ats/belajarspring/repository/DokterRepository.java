package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.DokterModel;

public interface DokterRepository extends JpaRepository<DokterModel, String>{
	
	@Query("select d from DokterModel d order By d.namaDokter ASC")
	List<DokterModel> urutNama();
	
	@Query("select d from DokterModel d where d.iDokter = 100")
	List<DokterModel> seratus();
}
