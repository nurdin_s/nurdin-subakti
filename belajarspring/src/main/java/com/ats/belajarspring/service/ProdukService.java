//package com.ats.belajarspring.service;
//
//import java.util.List;
//
//import javax.transaction.Transactional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.ats.belajarspring.model.ProdukModel;
//import com.ats.belajarspring.repository.ProdukRepository;
//
//@Service
//@Transactional
//public class ProdukService {
//
//	@Autowired
//	private ProdukRepository produkRepository;
//	
//	public void create(ProdukModel produkModel) {
//		produkRepository.save(produkModel);
//	}
//	
//	public List<ProdukModel> read(){
//		return produkRepository.findAll();
//	}
//	
//	public List<ProdukModel> search(String kataCari){
//		return produkRepository.QuerySelectAllWhereNama(kataCari);
//	}
//	
//	public List<ProdukModel> asc(){
//		return produkRepository.hargaProdukOrderAsc();
//	}
//}
