package com.ats.belajarspring.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ats.belajarspring.model.KaryawanModel;
import com.ats.belajarspring.repository.KaryawanRepository;

@Service
@Transactional
public class KaryawanService {

	@Autowired
	private KaryawanRepository karyawanRepository;
	
	/*
	 * C
	 * R
	 * U
	 * D
	 */
	
	public void created(KaryawanModel karyawanModel) {
		karyawanRepository.save(karyawanModel);
	}
	
	public List<KaryawanModel> read(){
		return karyawanRepository.findAll();
	}
	
	
}
